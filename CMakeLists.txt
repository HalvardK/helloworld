cmake_minimum_required(VERSION 3.0)

project (HelloWorld VERSION 1.0)

set ( HDRS
    functions.h)

set ( SRCS
    functions.cpp
    hello_world.cpp)

add_executable( ${CMAKE_PROJECT_NAME} ${HDRS} ${SRCS})
