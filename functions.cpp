#include "functions.h"
#include <iostream> //Pre processor directive


void helloWorld(const std:: string& message){

    std::cout << "Hello world, heed my words: " << message << std::endl;
}

void iShallNotSwear(){
    std::cout << "--MENU--\n"
                 "1. Show some stuff\n"
                 "2. Show some other stuff\n"
                 "3. Exit program\n";
}


void chooseElementInMenu(int input){

    switch (input){
        case 1: std::cout <<"Here is some stuff";
            break;
        case 2: std::cout <<"Here is some other stuff";
            break;
        case 3: std::cout <<"Exiting program...";
                std::terminate();
            break;

    }


}
